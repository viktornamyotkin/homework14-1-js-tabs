let $descriptions = $('.tabs-content__item');
let $tabs = $('.tabs');

$tabs.on('click', '.tabs-title', function (event) {
    $('.tabs-title').siblings().removeClass('active');
    $(event.target).addClass('active');
    let i = $(event.target).index();
    $descriptions.siblings().hide();
    $($descriptions[i]).show();
});